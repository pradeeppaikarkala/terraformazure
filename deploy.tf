# Configure the Microsoft Azure Provider
provider "azurerm" {
}

# Create a resource group if it doesn’t exist
resource "azurerm_resource_group" "terraformTest" {
    name     = "pradeep-myResourceGroup"
    location = "westus2"

    tags = {
        environment = "Terraform Demo"
    }
}

# Create virtual network
resource "azurerm_virtual_network" "pradeepterraformnetwork" {
    name                = "myVnet"
    address_space       = ["10.0.0.0/16"]
    location            = "westus2"
    resource_group_name = "${azurerm_resource_group.terraformTest.name}"

    tags = {
        environment = "Terraform Demo"
    }
}

# Create subnet
resource "azurerm_subnet" "pradeepterraformsubnet" {
    name                 = "mySubnet"
    resource_group_name  = "${azurerm_resource_group.terraformTest.name}"
    virtual_network_name = "${azurerm_virtual_network.pradeepterraformnetwork.name}"
    address_prefix       = "10.0.1.0/24"
}

# Create public IPs
resource "azurerm_public_ip" "pradeepterraformpublicip" {
    name                         = "myPublicIP"
    location                     = "westus2"
    resource_group_name          = "${azurerm_resource_group.terraformTest.name}"
    allocation_method            = "Dynamic"
    domain_name_label           = "pradeeptestapache"

    tags = {
        environment = "Terraform Demo"
    }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "pradeepterraformnsg" {
    name                = "myNetworkSecurityGroup"
    location            = "westus2"
    resource_group_name = "${azurerm_resource_group.terraformTest.name}"

security_rule {
    name                       = "HTTP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
   
 
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Demo"
    }
}

# Create network interface
resource "azurerm_network_interface" "pradeepterraformnic" {
    name                      = "myNIC"
    location                  = "westus2"
    resource_group_name       = "${azurerm_resource_group.terraformTest.name}"
    network_security_group_id = "${azurerm_network_security_group.pradeepterraformnsg.id}"

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = "${azurerm_subnet.pradeepterraformsubnet.id}"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = "${azurerm_public_ip.pradeepterraformpublicip.id}"
    }

    tags = {
        environment = "Terraform Demo"
    }
}

# Generate random text for a unique storage account name
/*resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.terraformTest.name}"
    }
    
    byte_length = 8
} */

# Create storage account for boot diagnostics
/* resource "azurerm_storage_account" "pradeepstorageaccount" {
    name                        = "diag${random_id.randomId.hex}"
    resource_group_name         = "${azurerm_resource_group.terraformTest.name}"
    location                    = "westus2"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        environment = "Terraform Demo"
    }
} */

# Create virtual machine
resource "azurerm_virtual_machine" "pradeepterraformvm" {
    name                  = "pradeepterraformvm"
    location              = "westus2"
    resource_group_name   = "${azurerm_resource_group.terraformTest.name}"
    network_interface_ids = ["${azurerm_network_interface.pradeepterraformnic.id}"]
    vm_size               = "Standard_B1ls"

delete_os_disk_on_termination = "true"
  
  storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    } 
   storage_os_disk {
        name              = "pradeepterraformOsDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    } 

   

os_profile {
computer_name = "pradeepterraformvm"
admin_username = "pradeep"
admin_password = "Amma@999"
}

    os_profile_linux_config {
        disable_password_authentication = false
        /*ssh_keys {
            path     = "/home/pradeep/.ssh/authorized_keys" 
            key_data = "ssh-rsa AAAAB3Nz{snip}hwhqT9h"
        } */
    }

    /*boot_diagnostics {
        enabled = "true"
        storage_uri = "${azurerm_storage_account.pradeepstorageaccount.primary_blob_endpoint}"
    } */

     # It's easy to transfer files or templates using Terraform.
  /*provisioner "file" {
    source      = "~/Terraform/files/setup.sh"
    destination = "/home/pradeep/"

    connection {
      type     = "ssh"
      user     = "pradeep"
      password = "Amma@999"
      host     = "${azurerm_public_ip.pradeepterraformpublicip.fqdn}" 
    }
  } */
 provisioner "file" { 
source      = "setup.sh"
destination = "/home/pradeep/setup.sh"
connection {
      type     = "ssh"
      user     = "pradeep"
      password = "Amma@999"
      host     = "${azurerm_public_ip.pradeepterraformpublicip.fqdn}" 
    }
}
  # This shell script starts our Apache server and prepares the demo environment.
  provisioner "remote-exec" {
    inline = [
     "chmod +x /home/pradeep/setup.sh",
     "sudo /home/pradeep/setup.sh",
    ]
connection {
      type     = "ssh"
      user     = "pradeep"
      password = "Amma@999"
      host     = "${azurerm_public_ip.pradeepterraformpublicip.fqdn}" 
    }

}

}




